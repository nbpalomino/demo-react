import React from 'react'

class Foobar extends React.Component {
    constructor(props) {
        super(props)
        this.state = { title: "Foobar", items: [1,2,3,4]}
    }

    render() {
        return (
            <div className="foo-bar">
                <h2>Im a {this.props.title} component. Old title {this.state.title}</h2>
                <ul>
                    {this.state.items.map(item => (
                        <li key={item.id}>{item}</li>
                    ))}
                </ul>
            </div>
        )
    }
}

export default Foobar